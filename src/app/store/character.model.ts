interface Conn {
    available: number;
    items: Item[];
}

interface Item {
    resourceURI: string;
    name: string;
}

export interface Character {
    id: number;
    name: string;
    description?: string;
    modified?: Date;
    thumbnail?: {
        path?: string;
        extension?: string;
    };
    comicts?: Conn;
    series?: Conn;
    stories?: Conn;
    events?: Conn;
    urls?: Array<{
        type: string;
        url: string;
    }>;
}