import { ActionsUnion, ActionTypes } from './actions';
import { Character } from './character.model';

export interface CharacterStore {
  characters: Character[];
  loading: boolean;
}

export const initialState: CharacterStore = {
  characters: [],
  loading: false,
};

export function CharacterReducer(state = initialState, action: ActionsUnion) {
  switch (action.type) {
    case ActionTypes.LoadCharacters:
      return {
        ...state,
        loading: true,
      };
    case ActionTypes.LoadSuccess:
      return {
        ...state,
        characters: [...state.characters, ...action.payload],
        loading: false,
      };
    default:
      return state;
  }
}
