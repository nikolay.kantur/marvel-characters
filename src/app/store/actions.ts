import { Action } from '@ngrx/store';
import { Character } from './character.model';

export enum ActionTypes {
  LoadCharacters =  '[Characters] Load characters from server',
  LoadSuccess =     '[Characters] Load success',
}

export class GetCharacters implements Action {
  readonly type = ActionTypes.LoadCharacters;
  constructor(public payload: { offset: number }) {}
}

export class LoadCharacters implements Action {
  readonly type = ActionTypes.LoadSuccess;
  constructor(public payload: Character[]) {}
}

export type ActionsUnion = LoadCharacters | GetCharacters;
