import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { EMPTY } from 'rxjs';
import { catchError, map, mergeMap } from 'rxjs/operators';
import { ActionTypes } from './actions';
import { CharacterService } from '../services/character.service';

@Injectable()
export class CharacterEffects {
  constructor(
    private actions$: Actions,
    private characterService: CharacterService,
  ) {}

  @Effect()
  loadProducts$ = this.actions$.pipe(
    ofType(ActionTypes.LoadCharacters),
    mergeMap(({ payload: { offset } }) =>
      this.characterService.getCharacters(offset).pipe(
        map(characters => {
          return { type: ActionTypes.LoadSuccess, payload: characters };
        }),
        catchError(() => EMPTY)
      )
    )
  );
}

