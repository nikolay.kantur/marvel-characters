import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, tap, distinctUntilChanged } from 'rxjs/operators';
import { Character } from '../store/character.model';

@Injectable({
  providedIn: 'root'
})
export class CharacterService {

  limit = 50;

  constructor(
    private http: HttpClient,
  ) { }

  getCharacters(offset = 0): Observable<Character[]> {
    const url = `http://gateway.marvel.com/v1/public/characters?offset=${offset}&limit=${this.limit}`;
    // nameStartsWith: 'girl', // TODO add search by names
    return this.http.get(url)
      .pipe(
        distinctUntilChanged(),
        map(res => (res as any).data.results), // TODO determine type
      );
  }

  getCharacter(characterId: string): Observable<Character> {
    const url = `http://gateway.marvel.com/v1/public/characters/${characterId}`;
    return this.http.get(url)
      .pipe(
        map(res => (res as any).data.results[0] || null)
      );
  }
}
