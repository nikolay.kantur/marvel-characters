import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CharacterService } from '../services/character.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { Location } from '@angular/common';

@Component({
  selector: 'app-character',
  templateUrl: './character.component.html',
  styleUrls: ['./character.component.scss']
})
export class CharacterComponent implements OnInit {

  characterId: string;
  character: any = null;

  constructor(
    private route: ActivatedRoute,
    public characterService: CharacterService,
    private spinner: NgxSpinnerService,
    private location: Location,
  ) {}

  ngOnInit(): void {
    this.spinner.show();

    this.route.paramMap.subscribe(params => {
      this.characterId = params.get('characterId');
      this.characterService.getCharacter(this.characterId)
        .subscribe(
          data => {
            this.character = data;
            this.spinner.hide();
          },
          error => {
            this.character = null;
            this.spinner.hide();
          }
        );
    });
  }

  goBack() {
    this.location.back();
  }

}
