import { Component, OnInit, OnDestroy } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { Store } from '@ngrx/store';
import { GetCharacters } from '../store/actions';
import { takeUntil } from 'rxjs/operators';
import { Character } from '../store/character.model';
import { Subject } from 'rxjs';
import { CharacterStore } from '../store/reducer';

@Component({
  selector: 'app-character-list',
  templateUrl: './character-list.component.html',
  styleUrls: ['./character-list.component.scss']
})
export class CharacterListComponent implements OnInit, OnDestroy {

  public url: string;
  public characters: Character[] = [];

  public loadMore = false;

  unsubscribe$: Subject<any> = new Subject<any>();

  constructor(
    public store: Store<{ characters: CharacterStore }>,
    private spinner: NgxSpinnerService,
  ) {
    this.store
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((data) => {
        this.characters = data.characters.characters;
        if (this.characters.length) {
          this.spinner.hide('primary');
        }

        const loading = data.characters.loading;
        if (!loading) { this.hideLoadMore(); }
      });
  }

  ngOnInit(): void {
    this.spinner.show('primary', {});
    this.store.dispatch(new GetCharacters({ offset: 0 }));
  }

  onScroll(): void {
    this.showLoadMore();
    this.store.dispatch(new GetCharacters({ offset: this.characters.length }));
  }

  showLoadMore() {
    this.spinner.show('loadMore', {});
    this.loadMore = true;
  }

  hideLoadMore() {
    this.spinner.hide('loadMore');
    this.loadMore = false;
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

}
